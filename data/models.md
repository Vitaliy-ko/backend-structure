### Bar models ###
* assortmentModel
* menuModel
* barOrdersModel

### Payment models ###
* creditModel
* promoCodesModel

### Equipment models ###
* catalogsModel
* equipmentBookingModel

### Trainings models ###
* subscriptionModel
* photosModel
* videosModel

### Social models ###
* blogModel

### Marketing models ###
* emailModel

### Engagement models ###
* achievementsModel

### Auth models ###
* usersModel (include admin etc...)