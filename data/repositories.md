### General ###
* baseRepository

### Bar repositories ###
* assortmentRepository
* menuRepository
* barOrdersRepository

### Payment repositories ###
* creditRepository
* promoCodesRepository

### Equipment repositories ###
* catalogsRepository
* equipmentBookingRepository

### Trainings repositories ###
* subscriptionRepository
* photosRepository
* videosRepository

### Social repositories ###
* blogRepository

### Marketing repositories ###
* emailRepository

### Engagement repositories ###
* achievementsRepository

### Auth repositories ###
usersRepository (include admin etc...)