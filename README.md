### General 

* As the business and application evolve, the DDD architecture is chosen for easy scaling.

* Protocols: http (for general application) and WebSocket (for implementing achievement notifications or adding new service)