### Bar services ###
* assortmentService
* menuService
* generateQRService
* generatePDFService
* barOrdersService

### Payment services ###
* creditService
* promoCodesService
* paymentMethodsService
* generateInvoiceService

### Equipment services ###
* catalogsService
* equipmentBookingService

### Trainings services ###
* subscriptionService
* photosService
* videosService

### Social services ###
* blogService

### Marketing services ###
* emailService

### Engagement services ###
* achievementsService

### Auth services ###
* authService
